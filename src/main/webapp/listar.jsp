<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib  uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
    <%@ taglib 	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>crudprod10 listar</title>
<style>
	.cabecera{margin:10px; padding: 10px;}
</style>
</head>
	
<body>
	<h1>Listado de productos: </h1>
	<!-- Con el tag de jsp usamos el id con el nombre de producto para referenciar os productos de la clase Producto10 -->
	<!-- Con la class sabemos que el producto es de la clase Producto10 -->
	
		
		
		

<br><br>
		
		
		<!-- Creamos una tabla para hacer un formato con los productos -->
<table>
	<tr class="cabecera"><td>C�digo</td><td>Nome</td><td>Prezo</td></tr>
		<!-- Bucle for each para recorrer el ArrayList<producto> -->
		
		<c:forEach var="producto" items="${lista}"> 
		<!-- Creamos urls para tener los values de los botones y saber que botones ha introducido el usuario -->
			<c:url var="url" value="/crudprod10">  
				<c:param name="comando" value="Editar"></c:param>
				<c:param name="idEnviar" value="${producto.getId()}"></c:param>;
				<c:param name="nome" value="${producto.getNome()}"></c:param>;
				<c:param name="prezo" value="${producto.getPrezo()}"></c:param>;
			</c:url>
			
			<c:url var="urlEliminar" value="/crudprod10">
				<c:param name="comando" value="Eliminar"></c:param>
				<c:param name="id" value="${producto.getId()}"></c:param>;
			</c:url>
			
				<c:url var="urlInsertar" value="/crudprod10">
				<c:param name="comando" value="Insertar"></c:param>
			</c:url>
			
			<!-- Formato tabla para que salgan el Id, nome y prezo -->
  			<tr><td>${producto.getId()}</td><td>${producto.getNome()}</td>    <!-- Sacamos el valor de cada producto con los getters  -->
  			<td> ${producto.getPrezo()}<a href="${url}"><button>Editar</button></a> <a href="${urlEliminar}"><button>Eliminar</button></a></td>
   		</tr>
</c:forEach>
</table>
<!-- Redirecciona a controlador con la url que creamos anteriormente. -->
<a href="${urlInsertar}"><button>Insertar</button></a>

	</body>
</html>