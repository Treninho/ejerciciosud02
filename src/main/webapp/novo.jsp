<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib  uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
    <%@ taglib 	uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insertar producto</title>
</head>
<!-- Aqu� es donde se registran nuevos productos. -->
<body>
<h2>Insertar novo producto</h2>


<!-- Creamos un formulario, el action manda al controlador para recoger los request de los input y poder usarlos -->

<form method="get" action="/crudprod10/crudprod10">
Nome: <input type="text" name="nome"><br>
Prezo: <input type="number" name="prezo" step="0.01"><br>
<button type="submit" name="comando" value="ins">Insertar</button>
</form>

</body>
</html>