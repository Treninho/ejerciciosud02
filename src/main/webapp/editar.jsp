<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
<h2>Editar producto</h2>

<!-- Aqu� es donde se edita los productos -->


<!-- formulario para redireccionar al controlador con el action y recoger los request de los input para poder usarlos -->
<form method="get" action="/crudprod10/crudprod10">
	Id:     ${param.idEnviar}<br>        <!-- As� saca el ID en la p�gina del producto una vez vamos a editarlo -->
	Nome: <input type="text" name="nome" value="${param.nome}"><br>  <!-- Muestra el nombre en el input del producto que estamos editando -->
	Prezo: <input type="number" name="prezo" step="0.01" value="${param.prezo}" ><br> <!--Muestra el prezo en el input del producto  -->
		<button type="submit" name="comando" value="editar">Actualizar</button> 
</form>
</body>
</html>