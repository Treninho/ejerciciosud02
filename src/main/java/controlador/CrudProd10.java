package controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.informaticapinguela.modelo.Producto10;
import com.informaticapinguela.modelo.ProductosDAO10;

/**
 * Servlet implementation class CrudProd10
 */


/**
 * 
 * @author David Rodríguez Trenas
 *
 */


/**
 * 
 * Con el WebServlet indicamos el nombre que hay que introducir en la url para acceder.
 *
 */
@WebServlet("/crudprod10")
public class CrudProd10 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// atributos requestDispatcher para poder reenviar a otros jsp
	RequestDispatcher rd;
	RequestDispatcher rd1;
	// listaProductos de tipo ProductoDAO10 para poder acceder a los métodos del modelo(ProductosDAO10)
	ProductosDAO10 listaProductos = new ProductosDAO10();
	int id;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CrudProd10() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    
    
    
    /**
     * Método que redirecciona a la página listar.jsp para mostrar datos de productos
     * 
     */
    

    /**
     * Método DoGet del Controlador donde gestiona los request y redirecciona a distinas paginas.jsp 
     * Llama a los métodos para realizar operaciones sobre el modelo.
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* recogemos el request para saber el botón que se ha pulsado
		 * con el c:param recogemos el name como nombre "comando"
		*/
		String boton = request.getParameter("comando");
		
		
		request.setAttribute("lista", listaProductos.getLista());  // recogemos el valor de la lista del modelo (ArrayList) y con listaProductos.getLista() obtenemos la carga de datos
		
		/**
		 * Switch donde sabe que botón ha introducido el usuario y realiza una acción
		 */
		
		// si el usuario ha pulsado un botón entra en el switch y depende del Value (Si es editar, borrar o insertar)realiza una acción u otra.
		if(boton!=null) {
			switch (request.getParameter("comando")) {
			case "Editar":
				id = Integer.parseInt(request.getParameter("idEnviar"));   // parseamos a int el request que nos devuelve el ID del producto (porque por defecto nos devuelve una string)
					// cuando pulsamos botón editar automaticamente redirecciona a a página editar.jsp
				rd = request.getRequestDispatcher("/editar.jsp");  
				rd.forward(request, response);
					// acaba el switch para que no se ejecute lo de abajo y no haya errores.
				break;
			case "Insertar":  // si el botón tiene como value "Insertar" entra en este case
				
					// si pulsamos botón de Insertar dentro de la página listar redirecciona a la página novo.jsp
				rd1 = request.getRequestDispatcher("/novo.jsp");   
				rd1.forward(request, response);
				// acaba el switch para que no se ejecute lo de abajo y no haya errores.
				break;
			case "Eliminar": 
					// listaProductos(tipo de ProductosDAO10) llama al método removeProducto y elimina un producto a través de un ID que viene gracias al c:param
					// que sabemos justamente el ID del bóton.
				listaProductos.removeProducto(Integer.parseInt((request.getParameter("id"))));
				// una vez eliminamos el producto, redirecciona a la página listar.jsp y actualiza la página con producto eliminado.
				rd = request.getRequestDispatcher("/listar.jsp");
				rd.forward(request, response);
					// acaba el switch para que no se ejecute lo de abajo y no haya errores.
				break;
			case "ins":  // Entra en este case el Botón Insertar de la pagina NOVO.JSP con su value de "ins"
					// listaProductos(tipo de ProductosDAO10) llama al método insertarProducto
					// que introducimos el nombre, y prezo de los input del formulario de Insertar producto (NOVO.jsp)
					// parseamos a Double el párametro de prezo ya que nos llega una String a través del request (por el input type)
				if (request.getParameter("nome")!= null) {
					listaProductos.insertarProducto(request.getParameter("nome"), Double.parseDouble(request.getParameter("prezo")));
					// una vez inserta producto, redirecciona a listar.jsp para actualizar página con producto insertado
					rd = request.getRequestDispatcher("/listar.jsp");
					rd.forward(request, response);
					// acaba el case y sale del swtich.
					break;
				}
				listaProductos.insertarProducto(request.getParameter("nome"), Double.parseDouble(request.getParameter("prezo")));
				// una vez inserta producto, redirecciona a listar.jsp para actualizar página con producto insertado
				rd = request.getRequestDispatcher("/listar.jsp");
				rd.forward(request, response);
				// acaba el case y sale del swtich.
				break;
			case "editar": // entra en este case si ha pulsado el botón Actualizar de la página 'editar.jsp'
				
					//listaProductos llama al método editar que introducimos como párametro de entrada:
					// el ID que recibimos del producto con el boton, que inicializamos arriba para meterle el valor del request. (Daba error si ponía request.getParameter("id"), porque desaparecía el ID)
				listaProductos.editar(id, request.getParameter("nome"), Double.parseDouble((request.getParameter("prezo"))));
				// redirecciona a listar.jsp una vez se ha editado el nombre y prezo.
				rd = request.getRequestDispatcher("/listar.jsp");
				rd.forward(request, response);
				// sale del switch
				break;
			}
			// si el botón llega ser nulo, cargamos la lista del modelo y redireccionamos a listar.jsp
		}else {
			request.setAttribute("lista", listaProductos.getLista());
			rd = request.getRequestDispatcher("/listar.jsp");
			rd.forward(request, response);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	
		
		}

		
}

