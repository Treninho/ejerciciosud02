package com.informaticapinguela.modelo;

/**
 * 
 * @author David Rodríguez Trenas
 * @version 3.1
 * 
 */


/**
 * Clase producto que se construye con un nombre y su precio
 * Cuando se instancia un new Producto, se incrementa un id
 *
 */

public class Producto10 {

	/**
	 * Atributos de la clase producto
	 */
	private String nome;
	private double prezo;
	private static int idClase=0;
	private  int id;
	
	
	/**
	 * Constructor sin parámetros de Producto10
	 * 
	 */
	
	/**
	 * Constructor de Producto 10
	 * @param nome
	 * @param prezo
	 */
	
	public Producto10() {
		
	}
	
	public Producto10(String nome, double prezo) {
		super();
		this.nome = nome;
		this.prezo = prezo;
		id=idClase++;
		
	}
	
	/**
	 * Nos devuelve el nombre del producto
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Cambia el nombre
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Devuelve el precio del producto
	 * @return prezo
	 */
	public double getPrezo() {
		return prezo;
	}
	/**
	 * Cambia el precio del producto
	 * @param prezo
	 */
	public void setPrezo(double prezo) {
		this.prezo = prezo;
	}
	/**
	 * Nos devuelve el id
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Devuelve todos los datos de los productos
	 */
	@Override
	public String toString() {
		return "Producto10 [nome=" + nome + ", prezo=" + prezo + ", id=" + id + "]";
	}
	
	
}
