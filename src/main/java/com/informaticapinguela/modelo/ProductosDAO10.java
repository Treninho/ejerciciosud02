package com.informaticapinguela.modelo;

import java.util.ArrayList;
import java.util.Iterator;

import controlador.CrudProd10;


/**
 * 
 * @author David Rodríguez Trenas
 *
 */



public class ProductosDAO10 {
	
	ArrayList<Producto10> lista = new ArrayList<Producto10>();
	
	/**
	 * Clase donde hace de Almacén (base de datos)
	 * 
	 */
	
	
	
	
	/**
	 * Devuelve los productos 
	 * @return ArrayList<Producto10>
	 */
		/*public  ArrayList<Producto10> almacenarProductos() {
			// añadimos nuevos productos a la lista 
			Producto10 p1 = new Producto10("Producto 1", 11.11);
			Producto10  p2 = new Producto10("Producto 2", 22.22);
			Producto10  p3 = new Producto10("Producto 3", 22.22);
			lista.add(p1);
			lista.add(p2);
			lista.add(p3);
			return lista;
		}
		*/ 
		
		public ProductosDAO10() {
			super();
			Producto10  p1 = new Producto10("Producto 1", 11.11);
			Producto10  p2 = new Producto10("Producto 2", 22.22);
			Producto10  p3 = new Producto10("Producto 3", 22.22);
			lista.add(p1);
			lista.add(p2);
			lista.add(p3);
	}

		public void insertarProducto(String nome, double prezo) {
			//double prezo1 = Double.parse(prezo);
			Producto10 producto4 = new Producto10(nome, prezo);
			lista.add(producto4);
			
		}
			
		public void removeProducto(int id) {
			Iterator<Producto10> it = lista.iterator();
			while(it.hasNext()) {
				Producto10 p = it.next();	
				if (p.getId()==id) {
					lista.remove(p);
				break;
				}
			}
				
		}
	
		public ArrayList<Producto10> getLista() {
			return lista;
		}

		public void editar(int id, String nome, double prezo) {
			Iterator<Producto10> it = lista.iterator();
			while(it.hasNext()) {
				Producto10 p = it.next();	
				if (p.getId()==id) {
					p.setNome(nome);
					p.setPrezo(prezo);
				break;
				}
			}
			
		}
	}

			

	
	
